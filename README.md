# Shitty test forum

[Original tutorial](https://code.tutsplus.com/tutorials/how-to-create-a-phpmysql-powered-forum-from-scratch--net-10188)

PHP7 and MySQL

Docker-compose stuff included, just run `docker-compose up`

After that, go to phpmyadmin at `http://localhost:4747`, log in with `user` and `pass`, click on db on the sidebar, click the SQL tab and copy and paste everything from `setup.sql`. Click on Go.

Access the site in `http://localhost:4545/forum/index.php`, create an account, make yourself admin by going to phpmyadmin again and running:
```sql
Update `users` Set `user_level` = 1 Where `user_id` = 1;
```
under the db -> SQL tab.

Create a category, then create a post.
