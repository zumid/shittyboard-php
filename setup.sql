-- create databases

Create Table `users`(
	`user_id`	Int(8)		Not Null	Auto_Increment,
	`user_name`	VarChar(32)	Not Null,
	`user_pass`	VarChar(255)	Not Null,
	`user_email`	VarChar(255)	Not Null,
	`user_date`	DateTime	Not Null,
	`user_level`	Int(4)	Not Null,
	Unique Index `username_unique` (`user_name`),
	Primary Key(`user_id`)
);

Create Table `replies`(
	`reply_id`	Int(8)		Not Null	Auto_Increment,
	`reply_content`	Text		Not Null,
	`reply_date`	DateTime	Not Null,
	`reply_topic`	Int(8)		Not Null,
	`reply_by`	Int(8)		Not Null,
	Primary Key(`reply_id`)
);

Create Table `categories`(
	`cat_id`		Int(8)		Not Null	Auto_Increment,
	`cat_name`		VarChar(255)	Not Null,
	`cat_description`	VarChar(255)	Not Null,
	Unique Index `cat_name_unique` (`cat_name`),
	Primary Key(`cat_id`)
);

Create Table `topics`(
	`topic_id`		Int(8)		Not Null	Auto_Increment,
	`topic_subject`		VarChar(255)	Not Null,
	`topic_date`		DateTime	Not Null,
	`topic_cat`	Int(8)		Not Null,
	`topic_by`	Int(8)		Not Null,
	Primary Key(`topic_id`)
);

Create Table `posts`(
	`post_id`	Int(8)		Not Null	Auto_Increment,
	`post_content`	Text		Not Null,
	`post_date`	DateTime	Not Null,
	`post_topic`	Int(8)		Not Null,
	`post_by`	Int(8)		Not Null,
	Primary Key(`post_id`)
);

Alter Table `topics`
	Add Foreign Key(`topic_cat`) References `categories`(`cat_id`)
		On Delete Cascade
		On Update Cascade;

Alter Table `posts`
	Add Foreign Key(`post_topic`) References `topics`(`topic_id`)
		On Delete Cascade
		On Update Cascade;

Alter Table `posts`
	Add Foreign Key(`post_by`) References `users`(`user_id`)
		On Delete Restrict
		On Update Cascade;
