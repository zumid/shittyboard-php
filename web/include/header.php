<!DOCTYPE html>
	<html>
		<head>
			<title>what's happening forum?</title>
		</head>
		<body>
		<div id="header">
			<h1>My forum</h1>
			<ul id="menu">
				<li><a href="/forum/index.php">Home</a></li>
				<li><a href="/forum/categories.php">Categories</a></li>
				<?php
					if( $_SESSION['signed_in'] == 1 ){
						echo '<li><a href="/forum/create_topic.php">Create a topic</a></li>';
					}
					if( $_SESSION['user_level'] > 0 ){
						echo '<li><a href="/forum/create_cat.php">Create a category</a></li>';
					}
				?>
			</ul>
			<div id="userbar">
			<?php
			    if($_SESSION['signed_in'])
			    {
				echo 'Hello ' . $_SESSION['user_name'] . '. Not you? <a href="signout.php">Sign out</a>';
			    }
			    else
			    {
				echo '<a href="signin.php">Sign in</a> or <a href="signup.php">create an account</a>.';
			    }
			?>
			</div>
		</div>
		<div id="content">
