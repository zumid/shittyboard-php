<?php
	session_start();
	// replace with PDO
	$pdo_host = 'db';
	$pdo_db   = 'db';
	$pdo_user = 'user';
	$pdo_pass = 'pass';

	$pdo_connection = "mysql:host=$pdo_host;dbname=$pdo_db";
	$pdo_options = [
		PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
	];

	try {
		$pdo = new PDO($pdo_connection, $pdo_user, $pdo_pass, $pdo_options);
	} catch (\PDOException $e) {
		echo $e->getMessage() . (int)$e->getCode();
	}
?>
