<?php
	require_once '../include/connect.php';
	require_once '../include/header.php';

	function show_form(string $uname){
		echo '
		<form method="post" action="">
			<label for="user_name">Username: </label>
			<input required type="text" id="user_name" name="user_name" value='.$uname.'>=
			<label for="user_pass">Password: </label>
			<input required type="password" id="user_pass" name="user_pass">=
			<input type="submit" value="Sign in">
		</form>
		';
	}

	echo '<h2>'.'Sign in'.'</h2>';
	
	if( isset($_SESSION['signed_in']) && $_SESSION['signed_in'] === 1 ){
		echo '<p>You are already signed in. Would you like to <a href="signout.php">sign out</a>?</p>';
	} else {
		if( $_SERVER['REQUEST_METHOD'] === 'POST' ){
			// form processing
			
			$errors = [];
			
			// check username
			if( (empty($_POST['user_name'])) ){
				$errors[] = 'Username: can\'t be empty!';
			}
			
			// check passwords
			if( (empty($_POST['user_pass'])) ){
				$errors[] = 'Password: can\'t be empty!';
			}
			
			// display errors
			if( !(empty($errors)) ){
				echo '<ul id="errors">';
				foreach($errors as $key => $error){
					echo '<li>'.$error.'</li>';
				}
				echo '</ul>';
				show_form($_POST['user_name']);
			} else {
			// no errors
				$sql = '
					Select
					    `user_id`,
					    `user_name`,
					    `user_level`,
					    `user_pass`
					    From `users`
					    Where `user_name` = :name
				';
				
				$username = $_POST['user_name'];
				$plain_pass = $_POST['user_pass'];
				$email = $_POST['user_email'];
				
				$query = $pdo->prepare($sql);
				$query->bindParam(':name', $username, PDO::PARAM_STR, 32);
				
				try{
					$query->execute();
					
					$new_user = $query->fetch();
					
					// can't find user
					if( empty($new_user) ){
						echo '<p>User not found.</p>';
						show_form($_POST['user_name']);
					} else {
						if ( !password_verify($plain_pass, $new_user['user_pass']) ){
							echo '<p>Invalid password.</p>';
							show_form($_POST['user_name']);
						} else {
						// successfully signed in
							$_SESSION['signed_in'] = 1;
							$_SESSION['user_id'] = $new_user['user_id'];
							$_SESSION['user_name'] = $new_user['user_name'];
							$_SESSION['user_level'] = $new_user['user_level'];
							echo '<p>Welcome. Proceed to <a href="index.php">overview</a>...</p>';
						}
					}
					
					
				} catch (PDOException $e){
					echo '<p>Something went wrong while signing in. Please try again later.</p>';
					echo '<p>';
					echo $e->getCode().' '.$e->getMessage(); // debug only
					echo '</p>';
				}
			}
		} else {
			// sign in form
			show_form("");
		}
	}

	require_once '../include/footer.php';
?>
