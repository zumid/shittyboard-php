<?php
	require_once '../include/connect.php';
	require_once '../include/header.php';

	function show_form(string $uname, string $email){
		echo '
		<form method="post" action="">
			<label for="user_name">Username: </label>
			<input required type="text" id="user_name" name="user_name" value='.$uname.'>
			<label for="user_email">Email: </label>
			<input required type="email" id="user_email" name="user_email" value='.$email.'>
			<label for="user_pass">Password: </label>
			<input required type="password" id="user_pass" name="user_pass">
			<label for="user_pass_check">Confirm password: </label>
			<input required type="password" id="user_pass_check" name="user_pass_check">
			<input type="submit" value="Sign up">
		</form>
		';
	}

	echo '<h2>'.'Sign up'.'</h2>';
	
	if( $_SERVER['REQUEST_METHOD'] === 'POST' ){
		// form processing
		
		$errors = [];
		
		// check username
		if( !(empty($_POST['user_name'])) ){
			if( !(ctype_alnum($_POST['user_name'])) ){
				$errors[] = 'Username: can only contain letters and digits.';
			}
			if( strlen($_POST['user_name']) > 32 ){
				$errors[] = 'Username: can\'t be longer than 32 characters.';
			}
		} else {
			$errors[] = 'Username: can\'t be empty!';
		}
		
		// check passwords
		if( !(empty($_POST['user_pass'])) ){
			if( $_POST['user_pass'] != $_POST['user_pass_check'] ){
				$errors[] = 'Password: two doesn\'t match.';
			}
		} else {
			$errors[] = 'Password: can\'t be empty!';
		}
		
		// check email
		if( (empty($_POST['user_email'])) ){
			$errors[] = 'E-mail: can\'t be empty!';
		} else {
			if( !filter_var($_POST['user_email'], FILTER_VALIDATE_EMAIL) )
			{
				$errors[] = 'E-mail: Invalid e-mail format!';
			}
		}
		
		// display errors
		if( !(empty($errors)) ){
			echo '<ul id="errors">';
			foreach($errors as $key => $error){
				echo '<li>'.$error.'</li>';
			}
			echo '</ul>';
			show_form($_POST['user_name'], $_POST['user_email']);
		} else {
		// no errors
			$sql = '
				Insert Into `users`(`user_name`, `user_pass`,
					            `user_email`, `user_date`,
					            `user_level`
					            )
				Values( :name, :pass, :email, Now(), 0 );
			';
			
			$username = $_POST['user_name'];
			$password = password_hash($_POST['user_pass'], PASSWORD_DEFAULT);
			$email = $_POST['user_email'];
			
			$query = $pdo->prepare($sql);
			$query->bindParam(':name', $username, PDO::PARAM_STR, 32);
			$query->bindParam(':pass', $password, PDO::PARAM_STR, 255);
			$query->bindParam(':email', $email, PDO::PARAM_STR, 255);
			
			try{
				$query->execute();
				
				echo '<p>Successfully registered. You can now <a href="signin.php">sign in</a> and start posting! :-)</p>';
			} catch (PDOException $e){
				echo '<p>Something went wrong while registering. Please try again later.</p>';
				echo '<p>';
				echo $e->getCode().' '.$e->getMessage(); // debug only
				echo '</p>';
			}
		}
	} else {
		// signup form
		show_form("","");
	}

	require_once '../include/footer.php';
?>
