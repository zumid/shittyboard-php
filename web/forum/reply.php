<?php
	require_once '../include/connect.php';
	require_once '../include/header.php';
	
	if( $_SERVER['REQUEST_METHOD'] === 'POST' ){
		if( $_SESSION['signed_in'] === 1 ){
			try{
				$sql = '
					Insert Into `posts`(
						`post_content`,
						`post_date`,
						`post_topic`,
						`post_by`)
					Values(
						:content,
						Now(),
						:id,
						:by
					);
					';
				$query = $pdo->prepare($sql);
				
				$reply_content = $_POST['reply-content'];
				$post_id = $_GET['id'];
				$user_id = $_SESSION['user_id'];
				
				$query->bindParam(':content', $reply_content, PDO::PARAM_STR);
				$query->bindParam(':id', $post_id, PDO::PARAM_STR);
				$query->bindParam(':by', $user_id, PDO::PARAM_STR);
				$query->execute();
				
				echo '<p>Your reply has been saved, back to the <a href="topic.php?id='.$post_id.'">topic</a></p>';
			} catch (PDOException $e){
				echo '<p>Oopsie woopsie, we made a fucky wucky. A wittle fucko boingo ;;w;;</p>';
				echo '<p>';
				echo $e->getCode().' '.$e->getMessage(); // debug only
				echo '</p>';
			}
			} else {
				echo '<p>You must be signed in to post a reply.</p>';
			}
	} else {
		echo '<p>This page can\'t be called directly.</p>';
	}

	require_once '../include/footer.php';
?>
