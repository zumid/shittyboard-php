<?php
	require_once '../include/connect.php';
	require_once '../include/header.php';

	function show_form(PDO $pdo, string $subject, string $catdesc){
		$sql = '
			Select `cat_id`, `cat_name`
			From `categories`;
			';
		$query = $pdo->prepare($sql);
		try{
			$query->execute();
			$categories = $query->fetchAll();
			
			if ( empty($categories) ){
				if($_SESSION['user_level'] > 0){
					echo '<p>You\'ve not created categories yet.</p>';
				} else {
					echo '<p>There are no categories yet. Wait for admin to make them.</p>';
				}
			} else {
				echo '
				<form method="post" action="">
					<label for="topic_subject">Subject: </label>
					<input required type="text" id="topic_subject" name="topic_subject" value="'.$subject.'">
					<label for="topic_cat">Category: </label>';
				echo '<select name="topic_cat">';
				foreach( $categories as $category ){
					echo '<option value="'.$category['cat_id'].'">'.$category['cat_name'].'</option>';
				}
				echo '</select>';
				echo '
					<label for="post_content">Message: </label>
					<textarea name="post_content" id="post_content"></textarea>
					<input type="submit" value="Add topic">
				</form>
				';
			}
		} catch (PDOException $e){
			echo '<p>Oopsie woopsie, we made a fucky wucky. A wittle fucko boingo ;;w;;</p>';
			echo '<p>';
			echo $e->getCode().' '.$e->getMessage(); // debug only
			echo '</p>';
		}
	}

	echo '<h2>'.'Create topic'.'</h2>';
	
	if( !isset($_SESSION['signed_in']) || $_SESSION['signed_in'] != 1 ){
		// not signed in
		echo 'Sorry, you have to be <a href="/forum/signin.php">signed in</a> to create a topic.';
	} else {
		if( $_SERVER['REQUEST_METHOD'] === 'POST' ){
			try{
			// topic query
				$sql = '
					Insert Into `topics`(`topic_subject`,
								`topic_date`,
								`topic_cat`,
								`topic_by`)
					Values( :subject, Now(), :cat, :by );
				';
				
				$subject = $_POST['topic_subject'];
				$cat = $_POST['topic_cat'];
				$by = $_SESSION['user_id'];
				
				$query = $pdo->prepare($sql);
				$query->bindParam(':subject', $subject, PDO::PARAM_STR);
				$query->bindParam(':cat', $cat, PDO::PARAM_STR);
				$query->bindParam(':by', $by, PDO::PARAM_STR);
				
				$query->execute();
				
			// post query
				$topic_id = $pdo->lastInsertId();
				
				$sql = '
					Insert Into `posts`(`post_content`,
								`post_date`,
								`post_topic`,
								`post_by`)
					Values( :content, Now(), :topic, :by );
				';
				
				$content = $_POST['post_content'];
				
				$query = $pdo->prepare($sql);
				$query->bindParam(':content', $content, PDO::PARAM_STR);
				$query->bindParam(':topic', $topic_id, PDO::PARAM_STR);
				$query->bindParam(':by', $by, PDO::PARAM_STR);
				
				$query->execute();
				
				echo '<p>The topic is here! <a href="topic.php?id='.$topic_id.'">Look!</a></p>';
			} catch (PDOException $e){
				echo '<p>Oopsie woopsie, we made a fucky wucky. A wittle fucko boingo ;;w;;</p>';
				echo '<p>';
				echo $e->getCode().' '.$e->getMessage(); // debug only
				echo '</p>';
			}
		
		} else {
			// signup form
			show_form($pdo, "","");
		}
	}

	require_once '../include/footer.php';
?>
