<?php
	require_once '../include/connect.php';
	require_once '../include/header.php';
	try{
		$cat_id = $_GET['id'];
		$sql = '
			Select `cat_id`, `cat_name`, `cat_description`
			From `categories`
			Where `cat_id` = :id;
			';
			
		$query = $pdo->prepare($sql);
		$query->bindParam(':id', $cat_id, PDO::PARAM_STR);
		$query->execute();
		$category = $query->fetch();
		
		if ( empty($category) ){
			echo '<p>Can\'t display category.</p>';
		} else {
			echo '<h2>Topics in '.$category['cat_name'].'</h2>';
			
			$sql = '
				Select `topic_id`, `topic_subject`, `topic_date`
				From `topics`
				Where `topic_cat` = :cat;
			';
			
			$query = $pdo->prepare($sql);
			$query->bindParam(':cat', $cat_id, PDO::PARAM_STR);
			$query->execute();
			$topics = $query->fetchAll();
			
			echo '<ul>';
			foreach( $topics as $topic ){
				echo '<li>';
				echo '<b><a href="topic.php?id='.$topic['topic_id'].'">'.$topic['topic_subject'].'</a></b>';
				echo '<br>';
				echo '<p>Created on '.$topic['topic_date'].'</p>';
				echo '</li>';
			}
			echo '</ul>';
		}
		
	} catch (PDOException $e){
		echo '<p>Oopsie woopsie, we made a fucky wucky. A wittle fucko boingo ;;w;;</p>';
		echo '<p>';
		echo $e->getCode().' '.$e->getMessage(); // debug only
		echo '</p>';
	}

	require_once '../include/footer.php';
?>
