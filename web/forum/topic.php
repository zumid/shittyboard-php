<?php
	require_once '../include/connect.php';
	require_once '../include/header.php';
	
	try{
		$post_id = $_GET['id'];
		$sql = '
			Select
				`topics`.`topic_id`,
				`topics`.`topic_subject`,
				`categories`.`cat_name`,
				`categories`.`cat_id`
			From `topics`
				Inner Join `categories` On `topics`.`topic_cat`=`categories`.`cat_id`
			Where `topics`.`topic_id` = :id;
			';
			
		$query = $pdo->prepare($sql);
		$query->bindParam(':id', $post_id, PDO::PARAM_STR);
		$query->execute();
		$topic = $query->fetch();
		
		if ( empty($topic) ){
			echo '<p>Can\'t display topic.</p>';
		} else {
			echo '<h2>Subject: '.$topic['topic_subject'].'</h2>';
			echo '<div id="returntocategory">Return to <a href="category.php?id='.$topic['cat_id'].'">'.$topic['cat_name'].'</a></div>';
			
			$sql = '
				Select
					`posts`.`post_content`,
					`posts`.`post_date`,
					`users`.`user_name`
				From `posts`
					Inner Join `users` On `posts`.`post_by` = `users`.`user_id`
				Where `posts`.`post_topic` = :id
				Order By `post_date` Asc;
			';
			
			$query = $pdo->prepare($sql);
			$query->bindParam(':id', $post_id, PDO::PARAM_STR);
			$query->execute();
			$posts = $query->fetchAll();
			
			echo '<ul>';
			foreach( $posts as $post ){
				echo '<li>';
				echo '['.$post['post_date'].'] &lt;'.$post['user_name'].'&gt; '.$post['post_content'];
				echo '</li>';
			}
			echo '</ul>';
			
			echo '<div id="reply">';
			echo '<h2>Write a reply</h2>';
			echo '<form method="post" action="reply.php?id='.$post_id.'">';
			echo '<textarea name="reply-content"></textarea>';
			echo '<input type="submit" value="Submit">';
			echo '</form>';
			echo '</div>';
		}
		
	} catch (PDOException $e){
		echo '<p>Oopsie woopsie, we made a fucky wucky. A wittle fucko boingo ;;w;;</p>';
		echo '<p>';
		echo $e->getCode().' '.$e->getMessage(); // debug only
		echo '</p>';
	}

	require_once '../include/footer.php';
?>
