<?php
	require_once '../include/connect.php';
	require_once '../include/header.php';

	function show_form(string $uname){
		echo '
		<form method="post" action="">
			<label for="user_name">Username: </label>
			<input required type="text" id="user_name" name="user_name" value='.$uname.'>=
			<label for="user_pass">Password: </label>
			<input required type="password" id="user_pass" name="user_pass">=
			<input type="submit" value="Sign in">
		</form>
		';
	}

	echo '<h2>'.'Sign out'.'</h2>';
	
	if( isset($_SESSION['signed_in']) && $_SESSION['signed_in'] === 1 ){
		$_SESSION = array();
		session_destroy();
		echo '<p>Goodbye. Proceed to <a href="index.php">overview</a>...</p>';
	} else {
		echo '<p>You are already signed out. Would you like to <a href="signin.php">sign in</a>?</p>';
	}

	require_once '../include/footer.php';
?>
