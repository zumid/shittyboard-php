<?php
	require_once '../include/connect.php';
	require_once '../include/header.php';
	$sql = '
		Select `cat_id`, `cat_name`, `cat_description`
		From `categories`;
		';
		
	$query = $pdo->prepare($sql);
	try{
		$query->execute();
		$categories = $query->fetchAll();
		
		if ( empty($categories) ){
			echo '<p>No categories defined yet.</p>';
		} else {
			echo '<ul>';
			foreach( $categories as $category ){
				echo '<li>';
				echo '<b><a href="category.php?id='.$category['cat_id'].'">'.$category['cat_name'].'</a></b>';
				echo '<br>';
				echo '<p>'.$category['cat_description'].'</p>';
				echo '</li>';
			}
			echo '</ul>';
		}
		
	} catch (PDOException $e){
		echo '<p>Oopsie woopsie, we made a fucky wucky. A wittle fucko boingo ;;w;;</p>';
		echo '<p>';
		echo $e->getCode().' '.$e->getMessage(); // debug only
		echo '</p>';
	}

	require_once '../include/footer.php';
?>
