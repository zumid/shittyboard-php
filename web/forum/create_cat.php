<?php
	require_once '../include/connect.php';
	require_once '../include/header.php';

	function show_form(string $catname, string $catdesc){
		echo '
		<form method="post" action="">
			<label for="cat_name">Category name: </label>
			<input required type="text" id="cat_name" name="cat_name" value='.$catname.'>
			<label for="cat_desc">Category description: </label>
			<textarea name="cat_desc">'.$catdesc.'</textarea>
			<input type="submit" value="Add category">
		</form>
		';
	}

	echo '<h2>'.'Add category'.'</h2>';
	
	if( !isset($_SESSION['signed_in']) || $_SESSION['signed_in'] != 1 ){
		// not signed in
		echo 'Sorry, you have to be <a href="/forum/signin.php">signed in</a> to create a category.';
	} else {
		if( $_SESSION['user_level'] == 0 ){
			echo 'Ya gotta be an admin to make a category.';
		} else {
			if( $_SERVER['REQUEST_METHOD'] === 'POST' ){
				$sql = '
					Insert Into `categories`(`cat_name`, `cat_description`)
					Values( :catname, :catdesc );
				';
				
				$catname = $_POST['cat_name'];
				$catdesc = $_POST['cat_desc'];
				
				$query = $pdo->prepare($sql);
				$query->bindParam(':catname', $catname, PDO::PARAM_STR, 255);
				$query->bindParam(':catdesc', $catdesc, PDO::PARAM_STR);
				
				try{
					$query->execute();
					
					echo '<p>New category added.</p>';
				} catch (PDOException $e){
					echo '<p>Oopsie woopsie, we made a fucky wucky. A wittle fucko boingo ;;w;;</p>';
					echo '<p>';
					echo $e->getCode().' '.$e->getMessage(); // debug only
					echo '</p>';
				}
			} else {
				// signup form
				show_form("","");
			}
		}
	}

	require_once '../include/footer.php';
?>
